#!/usr/bin/python
#
# Copyright 2014, The Regents of the University of Michigan.
#
# getdiskstatus.py - A tool to determine the CoreStorage status of a target Mac
#
# By default this tool runs verbosely which will print the CS status of
# the volumes found on the Mac it is run on including the physical and logical
# device names. In silent mode it will return a non-zero code to indicate
# status. To run in silent mode, use the -q switch.
#
# Possible statuses are:
#
# - No Fusion Drive, no FileVault               -> return code 2
# - No Fusion Drive, FileVault enabled          -> return code 3
# - Fusion Drive enabled, no FileVault          -> return code 4
# - Fusion Drive enabled, FileVault enabled     -> return code 5
#
# Verbose mode is useful for users who need to do a quick check for a
# given system while silent mode is useful for integration with other tools
# that perform further actions based on the return code.

import subprocess
import sys
import plistlib
import os

if not os.path.exists('/usr/sbin/system_profiler'):
    print 'Unable to locate system_profiler, exiting.'
    sys.exit(1)

cmd = ['/usr/sbin/system_profiler', 'SPStorageDataType', '-xml']

proc = subprocess.Popen(cmd, bufsize=-1, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
(pliststr, err) = proc.communicate()

if proc.returncode:
    print >> sys.stderr, 'Error: "%s" while running.' % err

try:
    csplist = plistlib.readPlistFromString(pliststr)[0]['_items'][0]
except:
    print >> sys.stderr, 'CoreStorage not active. Exiting.'
    sys.exit(1)
    
corestorage_pv = csplist['com.apple.corestorage.pv']
corestorage_fv2 = csplist['com.apple.corestorage.lv']['com.apple.corestorage.lv.encrypted']

def GetCSDetails():
    """docstring for GetCSDetails"""
    global quiet
    
    if len(corestorage_pv) < 2:
        if 'no' in corestorage_fv2:
            rc = 2
        else:
            rc = 3
    else:
        if 'no' in corestorage_fv2:
            rc = 4
        else:
            rc = 5
            
    if not quiet:
        if rc == 2:
            print '----------------------'
            print 'Fusion Drive = DISABLED'
            print 'FileVault    = DISABLED'
            print '----------------------'
        elif rc == 3:
            print '------------------------------------------------'
            print 'Fusion Drive = DISABLED'
            print 'FileVault    = ENABLED'
            print '------------------------------------------------'
        elif rc == 4:
            print '-----------------------------------------'
            print 'Fusion Drive = ENABLED'
            print 'FileVault    = DISABLED'
            print '-----------------------------------------'
        elif rc == 5:
            print '------------------------------------------'
            print 'Fusion Drive = ENABLED'
            print 'FileVault    = ENABLED'
            print '------------------------------------------'

        print 'Logical volume:'
        print '/dev/' + csplist['bsd_name']
        print '------------------'
        print 'Physical device(s):'
        for i in corestorage_pv:
            print i['_name'] + ' - ' + i['media_name']
        print '-------------------------------------------'

    return rc

quiet = None

if len(sys.argv) > 1:
    if '-q' in sys.argv[1]:
        quiet = True
    else:
        print 'Unrecognized option. Use -q to perform a silent run.'
        sys.exit(1)

def main():
    """docstring for main"""
    retcode = GetCSDetails()
    sys.exit(retcode)

if __name__ == '__main__':
    main()
